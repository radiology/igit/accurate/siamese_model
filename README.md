# siamese_model

Python code for the siamese model for collateral scoring, that was build and trained in the Accurate project, a research project in Erasmus MC and Quantib, funded by the Dutch Heart Foundation, and Health Holland.

The manuscript describing this method has been published in [Frontiers in Neuroimaging](https://doi.org/10.3389/fnimg.2023.1239703) :



> Fortunati V, Su J, Wolff L, van Doormaal P-J,
> Hofmeijer J, Martens J, Bokkers RPH,
> van Zwam WH, van der Lugt A and
> van Walsum T (2024) Siamese model for
> collateral score prediction from computed
> tomography angiography images in acute
> ischemic stroke.
> Front. Neuroimaging 2:1239703.
> doi: 10.3389/fnimg.2023.1239703


