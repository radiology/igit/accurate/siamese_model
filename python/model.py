import logging
from typing import Optional

import tensorflow as tf
import numpy as np
from keras import initializers
from keras import regularizers
from keras import backend as K
from keras import Model
from keras.layers import Layer, InputSpec
from keras import layers
from keras.layers import Activation, Add, Concatenate, Conv3D, Dense, Deconvolution3D, \
    GlobalMaxPooling3D, GlobalAveragePooling3D, Input, Multiply, ReLU, Subtract, UpSampling3D, \
    Reshape, Softmax, Permute, add, Lambda, Dropout, SpatialDropout3D, multiply

from .normalization_layers import GroupNormalization, InstanceNormalization


module_logger = logging.getLogger(__name__)

""" Fully convolutional networks for 3D images. """


class InstanceNormalizationAndActivation:
    """ Apply batch normalization and activation to input logits. """
    def __init__(self, activation):
        """
        Apply batch normalization before the activation. If the activation is a selu the batch
        normalization is not applied at all because normalization is implicitly applied via the
        activation function.
        Args:
            activation: Keras activation to use.
        """
        self._activation = activation

    def __call__(self, logits):
        """
        Args:
            logits: input to which apply batch normalization and activation.
        """
        normalization_layer = InstanceNormalization()
        return self._activation()(normalization_layer(logits))


def res_unit(n_filter: int, filter_shape: tuple, activation: Activation, weight_initializer: str,
             inputs: object, pre_convolution: bool = False) -> object:
    """
    Apply vox res net residual unit.

    Args:
        n_filter: number of features/convolutional filters.
        filter_shape: shape of the filter (width, height, depth).
        activation: Keras activation to be applied.
        weight_initializer: Keras weight initializer.
        pre_convolution: apply convolution to the input before the activation. This allows to apply
            residual learning independently to the number of input features.
        inputs: input tensor.

    Returns:
        output of the unit layer.
    """
    dimensions = len(filter_shape)
    convolution = getattr(layers, f'Conv{dimensions}D')
    if pre_convolution:
        inputs = convolution(n_filter, filter_shape, kernel_initializer=weight_initializer,
                             padding='same')(inputs)
    activations_1 = InstanceNormalizationAndActivation(activation)(inputs)

    logits_2 = convolution(n_filter, filter_shape, kernel_initializer=weight_initializer,
                           padding='same')(activations_1)
    activations_2 = InstanceNormalizationAndActivation(activation)(logits_2)

    logits_3 = convolution(n_filter, filter_shape, kernel_initializer=weight_initializer,
                           padding='same')(activations_2)
    residual = Add()([inputs, logits_3])
    return residual


def vox_res_net(input_shape: tuple, n_classes: int, downsample_factor: Optional[int] = 1,
                stride=None):
    """
    VoxResNet architecture model that implements a 3D residual network. The implementation follows
    the original caffe proto script. It uses same convolution (padded), batch normalization and
    up-convolution (transpose convolution).

    Args:
        input_shape: size of the input as a tuple (width, height, depth, channels).
            Since same convolution is used there is no margin as in U-Net. Possible input shapes
            are multiples of 8 for the network to work correctly.
        n_classes: number of output classes/channels. If more then 1 a softmax output layer is
            applied.
    Returns:
        untrained, un-compiled VoxResNet model.

    [1] VoxResNet: Deep voxel-wise residual networks for brain segmentation from 3D MR images.
        H. Chen et al. NeuroImage 2016.
    """
    inputs = Input(input_shape)
    for dimension in input_shape[:-1]:
        if dimension % 8 != 0:
            raise ValueError("Input size must be a multiple of 8.")
    activation = ReLU
    weight_initializer = 'glorot_normal'

    filter_shape = (3, 3, 3)
    strides = (2, 2, 2)
    # Apply stride first layer:
    stride_first = (1, 1, 1)
    if stride is not None:
        stride_first = stride

    n_features = 32
    if downsample_factor is None:
        downsample_factor = 1

    # Native scale level
    logits_1 = Conv3D(n_features, filter_shape, padding='same', strides=stride_first,
                      kernel_initializer=weight_initializer)(inputs)
    activations_1a = InstanceNormalizationAndActivation(activation)(logits_1)
    encoded_1 = Conv3D(n_features, filter_shape, padding='same',
                       kernel_initializer=weight_initializer)(activations_1a)

    # -> to Decoding 1
    activations_1b = InstanceNormalizationAndActivation(activation)(encoded_1)
    # Next scale level - input_size / 2 and first group of residual units.
    n_features *= 2
    logits_2 = Conv3D(n_features, filter_shape, strides=strides, padding='same',
                      kernel_initializer=weight_initializer)(activations_1b)
    residual_2 = res_unit(n_features, filter_shape, activation, weight_initializer, logits_2)
    encoded_2 = res_unit(n_features, filter_shape, activation, weight_initializer, residual_2)

    # -> to Decoding 2
    activations_3 = InstanceNormalizationAndActivation(activation)(encoded_2)
    # Next scale level input_size / 4 and second group of residual units.
    logits_3 = Conv3D(n_features, filter_shape, strides=strides, padding='same')(activations_3)
    residual_3 = res_unit(n_features, filter_shape, activation, weight_initializer, logits_3)
    encoded_3 = res_unit(n_features, filter_shape, activation, weight_initializer, residual_3)

    # -> to Decoding 3
    activations_4 = InstanceNormalizationAndActivation(activation)(encoded_3)
    # Next scale level input_size / 8 and third group of residual units.
    logits_4 = Conv3D(n_features, filter_shape, strides=strides, padding='same')(activations_4)
    residual_4 = res_unit(n_features, filter_shape, activation, weight_initializer, logits_4)
    encoded_4 = res_unit(n_features, filter_shape, activation, weight_initializer, residual_4)
    # -> to decoding 4

    # Classification at each resolution level. Get the encoded features, bring them to the
    # native scale and apply deeply supervised classification. All up-sampling and convolution
    # can be done more efficiently using deconvolution (transpose convolution).
    classifiers = []
    upsampling_strides = np.asarray((1, 1, 1))
    if downsample_factor < 2:
        # Decoding 1 (native scale 1)
        decoded_1 = Conv3D(n_classes, filter_shape, activation='relu', padding='same')(encoded_1)
        classifier_1 = Conv3D(n_classes, (1, 1, 1), activation='relu', padding='same')(decoded_1)
        classifiers.append(classifier_1)
        upsampling_strides *= 2

    if downsample_factor < 4:
        # Decoding 2 (Scale = input_size / 2)
        decoded_2 = Conv3D(n_classes, filter_shape, activation='relu', padding='same')(encoded_2)
        decoded_2 = UpSampling3D(size=upsampling_strides)(decoded_2)
        classifier_2 = Conv3D(n_classes, (1, 1, 1), activation='relu', padding='same')(decoded_2)
        classifiers.append(classifier_2)
        upsampling_strides *= 2

    if downsample_factor < 8:
        # Decoding 3 (Scale = input_size / 4)
        decoded_3 = Conv3D(n_classes, filter_shape, activation='relu', padding='same')(encoded_3)
        decoded_3 = UpSampling3D(size=upsampling_strides)(decoded_3)
        classifier_3 = Conv3D(n_classes, (1, 1, 1), activation='relu', padding='same')(decoded_3)
        classifiers.append(classifier_3)
        upsampling_strides *= 2

    # Decoding 4 (Scale = input_size / 8)
    decoded_4 = Conv3D(n_classes, filter_shape, activation='relu', padding='same')(encoded_4)
    decoded_4 = UpSampling3D(size=upsampling_strides)(decoded_4)

    classifier_4 = Conv3D(n_classes, (1, 1, 1), padding='same', activation='relu')(decoded_4)
    classifiers.append(classifier_4)

    # Combine classification and add final activation
    if len(classifiers) == 1:
        added = classifiers[0]
    else:
        added = Add()(classifiers)
    return Model(inputs, added)


def siamese_model(input_shape, pooling: bool = True, siamese_features: int = 32,
                  dichotomic: bool = False, downsample_factor: Optional[int] = None,
                  stride: Optional[list]=None) -> Model:
    """ Implements Siamese model with VoxResNet as a backbone using the Keras Model API.

    A VoxResNet is used for the symmetrical branches and each is followed by a global pooling
    layer. The feature maps from the VoxResNet are masked with the input mask, provided as input
    to the Siamese model. Then the masked outputs of the global pooling are concatenated and fed
    to a fully connected classifier that  softmax activation.

    The model expects 3 inputs: the first 2 the symmetrical input and the third input is a binary
    mask representing the region of interest to be considered for classification.

    Args:
        input_shape: shape of the input for each of the symmetrical side
        pooling: apply pooling. This option is useful to build a network to generate Class
            Activation Map by setting pooling to False.
        siamese_features: number of features output by each Siamese network.
        dichotomic: the output is dichotomic so it has one class (binary problem).
        downsample_factor: how much the backbone output is down-sampled with respect to the input.
        stride: stride applied to the first convolution. Used to compress the data.

    Returns:
        Siamese Keras model.

    """
    if downsample_factor is None:
        downsample_factor = 1
    stride = stride or [1, 1, 1]
    stride = tuple((1 / np.asarray(stride)).astype(int))

    input0 = Input(input_shape)
    input1 = Input(input_shape)
    mask_shape = np.asarray(input_shape) // (downsample_factor * np.asarray(stride + (1, )))
    mask_shape = np.maximum(1, mask_shape)
    mask = Input(tuple(mask_shape))
    vox_res_net_model = vox_res_net(input_shape, siamese_features, downsample_factor,
                                    stride=stride)
    backbone = vox_res_net_model
    output_0 = backbone(input0)
    output_1 = backbone(input1)
    output_0 = Multiply()([mask, output_0])
    output_1 = Multiply()([mask, output_1])


    if pooling:
        output_0 = GlobalAveragePooling3D()(output_0)
        output_1 = GlobalAveragePooling3D()(output_1)
    out = Concatenate(axis=-1)([output_0, output_1])

    nr_of_classes = 4
    activation = 'softmax'
    if dichotomic:
        nr_of_classes = 1
        activation = 'sigmoid'
    classifier = Dense(nr_of_classes, kernel_initializer='glorot_normal',
                       activation=activation)(out)
    return Model([input0, input1, mask], classifier)
